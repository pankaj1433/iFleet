import { combineReducers } from 'redux';

import {setConfig} from './AppInit'
import Login from './Login.reducer';
import Operations from './Operation.reducer'
import Assets from './Assets.reducer';
import Documents from './Documents.reducer';
import Loader from './Loader.reducer';

let rootReducer = combineReducers({
    config: setConfig,
    login: Login,
    operations: Operations,
    assets: Assets,
    documents: Documents,
    loader: Loader,
});

export default rootReducer;