const initialState = { 
    documentList: [],
    downloadToken: ""
};

export default (state = initialState, action) => {
    switch(action.type) {
        case 'GET_DOCUMENT_LIST': {
                return { ...state, documentList: action.data }
            return state;
        }
        case 'ADD_DOWNLOAD_TOKEN': {
                return { ...state, downloadToken: action.downloadToken }
            return state;
        }
        case 'UPLOAD_FILE_SUCCESS': {
            console.log('success',action)
        }
        default:
            return state;
    }
};