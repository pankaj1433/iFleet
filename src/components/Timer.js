import React, { Component } from 'react';
import moment from  "moment";
import { Text} from 'react-native';

class Timer extends React.Component{

    state = {
        endDate: moment().add(300, 'seconds'),
        countdown: '00:00',
        isExpired: false
    }

    componentWillMount () {
        this.tick();
        this.interval = setInterval(() => {
            this.tick()
        }, 1e3)
    }
    
    componentWillUnmount () {
        window.clearInterval(this.interval)
    }


    tick = () => {
        const {endDate} = this.state

        if (!endDate) {
            this.setState({
              countdown: '00:00'
            })
            return false
        }

        const now = moment()
        const diff = endDate.diff(now, 'seconds')

        if (diff <= 0) {
            this.setState({
              countdown: '00:00',
              isExpired: true
            })
            return false
        }

        const dur = moment.duration(diff, 'seconds')
        const countdown = `${dur.minutes()}:${dur.seconds()}`;
        
        this.setState({
            countdown,
            isExpired: false
        })

    }

    render() {
        return(
            <Text style={{color:'rgb(140,143,168)',fontFamily:'robotoRegular',fontSize:15}}>{this.state.countdown}</Text>     
        );
    }
}
export default Timer;