import React, { Component } from 'react';
import { View, Image, StatusBar, Text, StyleSheet, TextInput, TouchableOpacity, Dimensions, Platform } from 'react-native';
import { connect } from 'react-redux';
import { Feather } from '@expo/vector-icons';
import {Constants} from "expo";
const { width } = Dimensions.get('window');

const Header = (props) =>{
    let {title, withBack, navigation,hasLogout, logoutHandler,uploadButton,uploadDocument} = props;
    return(
        title ?
        <View style={styles.container}>
            {
                withBack &&
                <Feather onPress={()=>navigation.goBack()} style={styles.backIcon} name="chevron-left" size={30} color="#fff" />
            }
            {
                withBack ? 
                <Text style={styles.title}>{title}</Text> :
                <Text style={[styles.title,{marginLeft: 35}]}>{title}</Text> 
            }
            {
                hasLogout && 
                <View style={{flex:1,alignItems:'flex-end'}}>
                    <Feather onPress={()=>logoutHandler()} style={styles.rightMenu} name="more-vertical" size={28} color="#f0f2f3" />
                </View>
            }
            {
                uploadButton &&
                <TouchableOpacity onPress={()=>uploadDocument()} style={{flex:1,justifyContent:'flex-end',flexDirection:'row',alignItems:'center'}}>
                    <Feather style={styles.rightMenu} name="upload" size={20} color="#86D5A5" />
                    <Text style={{fontSize: 15,color : 'rgb(134, 213, 165)',fontFamily:'robotoRegular'}}>Upload</Text>
                </TouchableOpacity>
            }
        </View>
        :null
    );
}


const styles = StyleSheet.create({
    container: {
        width: width,
        flexDirection: 'row',
        height: 60,
        backgroundColor: '#3688cc',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingRight: 20,
        ...Platform.select({
            ios: {
                marginTop: Constants.statusBarHeight,
            },
            android: {
                marginTop: Constants.statusBarHeight,
            }
        })
    },
    title: {
        color: '#f0f2f3',
        fontSize: 20,
        fontFamily: 'robotoRegular'
    },
    backIcon: {
        paddingRight: 5,
        paddingVertical: 5,
    },
    rightMenu: {
        paddingRight: 5,
        paddingVertical: 5,
        alignContent: 'flex-end'
    }
})

export default Header;