import React, { Component } from 'react';
import { View,  Image, StyleSheet, Text, Animated, ActivityIndicator, Dimensions } from 'react-native';
import { connect } from 'react-redux';
const { width } = Dimensions.get('window');

class Loader extends Component {

    constructor(){
        super();
        this.state= {
            fadeAnim: new Animated.Value(0),
        }
    }

    componentWillMount() {
        Animated.timing(
            this.state.fadeAnim,
            {
                toValue: 1,
                duration: 1000
            }
        ).start()
    }

    componentWillUnmount() {
        Animated.timing(
            this.state.fadeAnim,
            {
                toValue: 0,
                duration: 1000
            }
        ).start()

    }

    render() {
        console.log('in loader', this.props.visible);
        let { fadeAnim } = this.state; 
        if(this.props.visible) {
            return (
                <Animated.View style={{flex:1,position:'absolute',top:0,bottom:0,left:0,right:0,zIndex:999,opacity: fadeAnim, backgroundColor: '#fff',}}>
                    <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                        <ActivityIndicator size="large" color="#3688cc" />
                    </View>
                </Animated.View>
                )
        }
        else {
            return null;
        }
    };
};

const styles = StyleSheet.create({
    container: {
        flex:1,
        alignItems: 'center',
    },
    operationWrapper: {
        width: width,
        backgroundColor: '#fff',
        flex:1,
    },
    sectionTitle: {
        width: width,
        flexDirection: 'row',
        height: 45,
        backgroundColor: '#2f6da1',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderTopWidth: 1,
        borderColor: '#344e94'
    },
})

const mapDispatchToProps = () => ({   
});

const mapStateToProps = (state) => ({
    visible: state.loader.visible
});

export default connect(mapStateToProps, mapDispatchToProps)(Loader);