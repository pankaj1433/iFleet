import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, ScrollView, ActivityIndicator  } from 'react-native';
import { connect } from 'react-redux';
const { width } = Dimensions.get('window');

//components
import Header from '../components/Header';
import DocumentList from '../components/DocumentList';
import Loader from '../components/Loader';
import { ImagePicker } from 'expo';

//actions
import {getDocumentsList} from '../actions/documents.action';
import {getDownloadToken,deleteDocument,uploadFile} from '../actions/file.action';

class Documents extends Component {

    constructor() {
        super();
        this.state = {
            loader: true,
        }
    }

    componentWillMount() {
        let { assetID, operationID } = this.props.navigation.state.params;
        this.props.getDocumentsList(assetID, operationID);
    }

    uploadDocument = async () => {
        let { assetID, operationID } = this.props.navigation.state.params;
        let result = await ImagePicker.launchImageLibraryAsync({
          allowsEditing: true,
          mediaTypes: 'All'
        });
    
        console.log('resultoperationID',operationID);
    
        if (!result.cancelled) {
          this.setState({ image: result.uri },()=>{
            let uriParts = result.uri.split('.');
            let fileType = uriParts[uriParts.length - 1];

            let uriPart = result.uri.split('/');
            let fileName = uriPart[uriPart.length-1];
            const data = new FormData();
           
            data.append('file', {
                uri:this.state.image.uri,
                'name': fileName,
            });
            this.props.uploadFile(assetID,operationID,data)
          });
        }
    };

    deleteHandler = (id) => {
        this.props.deleteDocument(id);
    }

    downloadHandler = (id) => {
        this.props.getDownloadToken(id);
    }

    renderList = (list) => {
        return list.map((listItem, index) => 
        !listItem.deleted &&
            <DocumentList 
                id = {listItem.id}
                key={index} 
                downloadHandler={this.downloadHandler} 
                deleteHandler={this.deleteHandler}
                title={listItem.name} 
                isDirectory = {listItem.directory || false}
                file = {listItem.file || null}
            />
        )
    }
    
    render() {
        let {documents, navigation} = this.props;
        console.log(documents,'documents>>>');
        return(
            <View style={styles.container}>
                <Loader/>
                <View style={styles.operationWrapper}>
                    <Header 
                        navigation={navigation} 
                        withBack={true} 
                        uploadButton={true}
                        uploadDocument = {this.uploadDocument}
                        title={navigation.state.params.title} />
                    {   
                        documents.length > 0 &&
                        <ScrollView style={styles.scrollWrapper}>
                            <View>
                                <View style={styles.sectionTitle}>
                                    <Text style={styles.sectionTitleText}>{documents.length || 0} Documents</Text>
                                    {/* <Text style={styles.downloadText}>Download All</Text> */}
                                </View>
                                {
                                    documents.length &&
                                    this.renderList(documents)
                                }
                            </View>
                        </ScrollView>
                    }
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        alignItems: 'center',
    },
    operationWrapper: {
        flex:1,
        width: width,
        backgroundColor: '#fff',
    },
    sectionTitle: {
        width: width,
        flexDirection: 'row',
        height: 40,
        backgroundColor: '#2f6da1',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: 35,
        paddingRight: 30,
    },
    sectionTitleText: {
        color: '#f0f2f3',
        fontSize: 16
    },
    downloadText: {
        color : 'rgb(134, 213, 165)',

    }
})

const mapStateToProps = (state) => ({
    documents: state.documents.documentList,
    downloadToken: state.documents.downloadToken
})

const mapDispatchToProps = (dispatch) => ({
    getDocumentsList: (assetID, operationID) => dispatch(getDocumentsList(assetID, operationID)),
    getDownloadToken: (documentID) => dispatch(getDownloadToken(documentID)),
    deleteDocument: (documentID) => dispatch(deleteDocument(documentID)),
    uploadFile: (assetID, operationID,file) => dispatch(uploadFile(assetID, operationID,file)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Documents);