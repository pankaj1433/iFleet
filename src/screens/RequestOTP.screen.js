import React, { Component } from 'react';
import { View, Image, Text, StyleSheet, TouchableOpacity, Dimensions, Platform, Alert  } from 'react-native';
import { connect } from 'react-redux';
import { Feather } from '@expo/vector-icons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const { width } = Dimensions.get('window');

//actions
import { generateOTP } from '../actions/login.action';

//components
import Loader from '../components/Loader'

class RequestOTP extends Component {


    constructor () {
        super();
        this.state = {
            selectedMobile: "",
            showAlert: false,
        }
    }

    componentWillReceiveProps(nextProps) {
        let {userName, mobiles} = nextProps.navigation.state.params;
        if(nextProps.otpStatus) {
            nextProps.navigation.navigate('EnterOTP', {
                mobiles: this.state.selectedMobile,
                userName: userName
            });
        }
    }

    requestOTP = () => {
        let payload = {
            "number": this.state.selectedMobile, 
            "username": this.props.navigation.state.params.userName 
            };
        this.props.generateOTP(payload);
    }

    showAlert = () => {
       this.setState = {
           showAlert: true
       }
    }

    renderMobileNumbers = (mobiles) => {
        if(mobiles && mobiles.length > 0) {
            return mobiles.map((mobile,index) => 
                    <TouchableOpacity style={styles.mobileRow}  key = {index} onPress={() => this.setState({showAlert: true, selectedMobile: mobile.number})} >
                        {
                            this.state.selectedMobile == mobile.number ?
                            <Feather style={{marginRight: 10}} name="disc" size={20} color='#8a9ed0' />:
                            <Feather style={{marginRight: 10}} name="circle" size={20} color='#8a9ed0' />    
                        }
                        <Text style={styles.lightText}>{mobile.number}</Text>
                    </TouchableOpacity>
            )
        }
    }

    addNumber = () => {
        let {userName} = this.props.navigation.state.params;
        this.props.navigation.navigate('AddNumber', {
            userName: userName
        });
    }

    render() {
        let {userName, mobiles} = this.props.navigation.state.params;
        return(
            <View style={{flex:1,alignItems: 'center',position:'relative'}}>
                <View style={styles.container}>
                    {
                        this.state.showAlert &&
                        <View style = {{flex:1,backgroundColor:'rgba(255,255,255,0.65)',position:'absolute',top:0,bottom:0,left:0,right:0,zIndex:100}}>
                            <View style={{flex:1,alignItems:'center',justifyContent:'center',paddingHorizontal:50}}>
                                <View style={{borderTopLeftRadius:7,borderTopRightRadius :7, width:width-100,backgroundColor:'rgb(140,143,168)',paddingVertical: 35,paddingHorizontal:15}}>
                                    <Text style={{color:'#fff',fontFamily: 'robotoRegular',fontSize:16,textAlign:'center'}}>You will be receiving an OTP on</Text>
                                    <Text style={{color:'#fff',fontFamily: 'robotoRegular',fontSize:16,textAlign:'center',marginTop:15}}>{this.state.selectedMobile}</Text>
                                </View>
                                <View style={{width:width-100,marginTop:2,flexDirection:'row'}}>
                                    <TouchableOpacity onPress={()=>this.setState({showAlert: false})} style={{borderBottomLeftRadius:7, backgroundColor:'rgb(140,143,168)',flex:1,marginRight:2,paddingVertical:15}}>
                                        <Text style={{color:'#fff',fontFamily: 'robotoRegular',fontSize:16,textAlign:'center'}}>Cancel</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={()=>this.requestOTP()} style={{borderBottomRightRadius:7, backgroundColor:'rgb(140,143,168)',flex:1,paddingVertical:15}}>
                                        <Text style={{color:'#fff',fontFamily: 'robotoRegular',fontSize:16,textAlign:'center'}}>OK</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    }
                    <View style = {styles.topTextWrapper}>
                        <Text style={styles.topText}>Please select the number you want to receive an OTP on</Text>
                    </View>
                    <View style = {styles.bottomWrapper}>
                        <View style = {styles.radioWrapper}>
                            <View style={{borderBottomWidth:1,borderColor: '#8a9ed0',padding:10}}>
                                <Text style={styles.lightText}>{userName}</Text>
                            </View>
                            <View style={{padding:20}}>
                                {this.renderMobileNumbers(mobiles)}
                            </View>
                        </View>
                    </View>
                    <View style={[styles.topTextWrapper,{justifyContent:'flex-start'}]}>
                        <TouchableOpacity onPress={()=>this.addNumber()}>
                            <View style={{flexDirection: 'row',alignItems:'center',justifyContent:'center'}}>
                                <Feather style={{marginRight: 10}} name="plus-circle" size={40} color='#f92b2d' />
                                <Text style={{fontSize:16,fontFamily:'robotoRegular',marginLeft:5,color:'#f92b2d'}}>Add new Number</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                <Loader/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        alignItems: 'center',
        backgroundColor: '#fff',
        padding: 30,
        paddingBottom: 40,
    },
    topTextWrapper: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: width - 60,
    },
    bottomWrapper: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: width - 60,
    },
    topText: {
        fontSize: 16,
        fontFamily: 'robotoRegular',
        textAlign: 'center'
    },
    radioWrapper: {
        borderWidth: 1,
        borderColor: '#8a9ed0',
        width: width - 60,
    },
    lightText: {
        fontSize: 15,
        fontFamily: 'robotoRegular',
        color: '#8c8fa8'
    },
    mobileRow: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center'
    }
})

const mapStateToProps = (state) => ({
    otpStatus: state.login.otpStatus,
})

const mapDispatchToProps = (dispatch) => ({
    generateOTP: (payload) => dispatch(generateOTP(payload)),
})

export default connect(mapStateToProps, mapDispatchToProps)(RequestOTP);