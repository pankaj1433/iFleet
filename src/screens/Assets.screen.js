import React, { Component } from 'react';
import {
	View,
	Text,
	StyleSheet,
	Dimensions,
	ScrollView,
	ActivityIndicator,
	FlatList,
	Button,
	Image,
} from 'react-native';
import { connect } from 'react-redux';
const { width } = Dimensions.get('window');

//components
import Header from '../components/Header';
import List from '../components/List';
import Loader from '../components/Loader';

//actions
import { getAssetsList } from '../actions/assets.action';

class Assets extends Component {
	state = {
		image: null,
	};

	componentWillMount() {
		this.props.getAssetsList(this.props.navigation.state.params.operationID);
	}

	listTapHandler = (title, operationID, assetID) => {
		this.props.navigation.navigate('Documents', {
			title,
			operationID,
			assetID,
		});
	};

	renderList = (list) => {
		return list.map((listItem, index) => {
			let {operationID} = this.props.navigation.state.params;
			let { name, total_claims, id } = listItem;
			let voyage =
				total_claims == 0
					? 'No Vovages'
					: total_claims == 1 ? total_claims + ' Voyage' : total_claims + ' Voyages';
			return (
				<List
					key={index}
					handler={(title, operationID, assetID) =>
						this.listTapHandler(name, this.props.navigation.state.params.operationID, id)}
					title={name}
					byLine={voyage}
					paddingVertical={25}
				/>
			);
		});
	};

	renderAsset = (title, list) => {
		return (
			<View>
				<View style={styles.sectionTitle}>
					<Text style={styles.sectionTitleText}>{title}</Text>
				</View>
				{this.renderList(list)}
			</View>
		);
    };
    
	render() {
		return (
			<View style={styles.container}>
				<Loader />
				<Header
					navigation={this.props.navigation}
					withBack={true}
					title={this.props.navigation.state.params.title}
                />
				{this.props.assets.length > 0 && (
					<ScrollView style={styles.scrollWrapper}>
						<View style={styles.scrollWrapper}>
							<View>
								{this.props.assets &&
									this.props.assets[0].mobile_assets &&
									this.props.assets[0].mobile_assets.length > 0 &&
									this.renderAsset(
										'Mobile Assets',
										this.props.assets[0].mobile_assets
									)}
							</View>
							<View>
								{this.props.assets &&
									this.props.assets[0].stationary_assets &&
									this.props.assets[0].stationary_assets.length > 0 &&
									this.renderAsset(
										'Stationary Assets',
										this.props.assets[0].stationary_assets
									)}
							</View>
						</View>
					</ScrollView>
				)}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		backgroundColor: '#fff',
	},
	scrollWrapper: {
		flex: 1,
	},
	sectionTitle: {
		width: width,
		flexDirection: 'row',
		height: 45,
		backgroundColor: '#2f6da1',
		justifyContent: 'flex-start',
		alignItems: 'center',
		borderTopWidth: 1,
		borderColor: '#344e94',
	},
	sectionTitleText: {
		marginLeft: 35,
		color: '#f0f2f3',
		fontSize: 18,
		fontFamily: 'robotoRegular',
	},
});

const mapStateToProps = (state) => ({
	assets: state.assets.data,
});

const mapDispatchToProps = (dispatch) => ({
	getAssetsList: (id) => dispatch(getAssetsList(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Assets);
