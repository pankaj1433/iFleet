import React, { Component } from 'react';
import { View, Image, Text, StyleSheet, TouchableOpacity, Dimensions, Platform, Alert, TextInput  } from 'react-native';
import { connect } from 'react-redux';
import { Feather } from '@expo/vector-icons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const { width } = Dimensions.get('window');

//actions
import { generateOTP } from '../actions/login.action';

//components
import Loader from '../components/Loader'

class AddNumber extends Component {


    constructor () {
        super();
        this.state = {
            selectedMobile: "",
            countryCode: ""
        }
    }

    componentWillReceiveProps(nextProps) {
        let {userName} = nextProps.navigation.state.params;
        if(nextProps.otpStatus) {
            nextProps.navigation.navigate('EnterOTP', {
                mobiles: "+"+this.state.countryCode+this.state.selectedMobile,
                userName: userName
            });
        }
    }

    requestOTP = () => {
        let payload = {
            "number": "+"+this.state.countryCode+this.state.selectedMobile, 
            "username": this.props.navigation.state.params.userName 
        };
        this.props.generateOTP(payload);
    }

    render() {
        let {userName, mobiles} = this.props.navigation.state.params;
        return(
            <View style={styles.container}>
                <Loader/>
                <View style = {styles.topTextWrapper}>
                        <Text style={styles.topText}>Enter new number here for OTP verification</Text>
                </View>
                <View style = {styles.bottomWrapper}>
                    <Text style={{color:'rgb(140,143,168)',fontFamily:'robotoRegular',fontSize:16}}>Please enter OTP below</Text>
                    <View style={{flexDirection:'row',alignItems:'center',justifyContent:'center',marginVertical:20}}>
                        <Text style={{fontSize:18,color:'#8798c5'}}>+</Text>
                        <TextInput
                            ref='otpField'
                            style={[styles.userfieldInput,{borderBottomColor: '#8a9ed0',borderBottomWidth: 2,width:45}]}
                            onChangeText={(countryCode) => { this.setState({ countryCode }) }}
                            value={this.state.countryCode}
                            underlineColorAndroid={'transparent'}
                            keyboardType='numeric'
                            returnKeyType='done'
                            maxLength = {3}
                        />
                        <Text style={{fontSize:18,color:'#8798c5'}}>-</Text>
                        <TextInput
                                ref='otpField'
                                style={[styles.userfieldInput,{borderBottomColor: '#8a9ed0',borderBottomWidth: 2}]}
                                onChangeText={(selectedMobile) => { this.setState({ selectedMobile }) }}
                                value={this.state.selectedMobile}
                                underlineColorAndroid={'transparent'}
                                keyboardType='numeric'
                                returnKeyType='done'
                                maxLength = {10}
                            />
                    </View>
                    <TouchableOpacity onPress={this.requestOTP} style={styles.loginButton}>
                        <Text style={styles.loginButtontext}>
                            Generate OTP
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    userfieldInput: {
        color: '#8798c5',
        width: width/2,
        fontSize: 18,
        fontFamily: 'robotoLight',
        // textAlign: 'center',
        height: 30,
        marginLeft: 20,
        letterSpacing: 0,
        marginHorizontal: 20,
    },
    topTextWrapper: {
        paddingVertical: 100,
        justifyContent: 'center',
        alignItems: 'center',
        width: width - 60,
    },
    bottomWrapper: {
        flexShrink: 1,
        width: width - 60,
        justifyContent:'center',
        alignItems: 'center',
    },
    topText: {
        fontSize: 16,
        fontFamily: 'robotoRegular',
        textAlign: 'center'
    },
    loginButtontext: {
        color: '#fff',
        fontSize: 20,
        fontFamily: 'robotoRegular',
    },
    loginButton: {
        paddingVertical: 15,
        width: width - 80,
        alignItems: 'center',
        backgroundColor: '#1c5a94',
        justifyContent: 'center',
        marginTop: 60,
        shadowOffset: {  
            width: 1,  
            height: 1,  
        }, 
        shadowColor: 'black',
        shadowOpacity: 0.3, 
        elevation: 2
    }
})

const mapStateToProps = (state) => ({
    otpStatus: state.login.otpStatus,
})

const mapDispatchToProps = (dispatch) => ({
    generateOTP: (payload) => dispatch(generateOTP(payload)),
})

export default connect(mapStateToProps, mapDispatchToProps)(AddNumber);