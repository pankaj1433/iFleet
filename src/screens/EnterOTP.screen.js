import React, { Component } from 'react';
import { View, Image, Text, StyleSheet, TouchableOpacity, TextInput, Dimensions, Platform  } from 'react-native';
import { connect } from 'react-redux';
import { Feather } from '@expo/vector-icons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Constants } from "expo";

const { width } = Dimensions.get('window');

//actions
import { verifyOTP } from '../actions/login.action'
import Loader from '../components/Loader';

//components
import Timer from '../components/Timer';

class EnterOTP extends Component {


    constructor () {
        super();
        this.state = {
            otp: "",
        }
    }

    verifyOTP = () => {
        let {userName, mobiles} = this.props.navigation.state.params;

        let payload = {
                            "number": mobiles, 
                            "username": userName, 
                            "otp": this.state.otp, 
                            "deviceid": Constants.deviceId, 
                            "devicename": Constants.deviceName, 
                            "deviceos": Platform.OS, 
                            "deviceosversion": Platform.Version.toString()
                        }
        this.props.verifyOTP(payload);
            
    }

    render() {
        let {userName, mobiles} = this.props.navigation.state.params;
        return(
            <View style={styles.container}>
                <Loader/>
                <View style = {styles.topTextWrapper}>
                        <Text style={styles.topText}>We have sent an OTP to your registered phone number / email for verification</Text>
                </View>
                <View style = {styles.bottomWrapper}>
                    <Text style={{color:'rgb(140,143,168)',fontFamily:'robotoRegular',fontSize:16}}>Please enter OTP below</Text>
                    <View style={{alignItems:'center',justifyContent:'center',borderBottomColor: '#8a9ed0',borderBottomWidth: 2,marginVertical:20}}>
                        <TextInput
                                ref='otpField'
                                style={styles.userfieldInput}
                                onChangeText={(otp) => { this.setState({ otp }) }}
                                value={this.state.otp}
                                underlineColorAndroid={'transparent'}
                                placeholder="OTP"
                                placeholderTextColor='#8798c5'
                                keyboardType='numeric'
                                returnKeyType='done'
                                maxLength = {6}
                            />
                    </View>
                    <Timer/>
                    <TouchableOpacity onPress={this.verifyOTP} style={styles.loginButton}>
                        <Text style={styles.loginButtontext}>
                            Verify OTP
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        alignItems: 'center',
        backgroundColor: '#fff',
        position:'relative'
    },
    userfieldInput: {
        color: '#8798c5',
        width: width/3,
        fontSize: 20,
        fontFamily: 'robotoLight',
        textAlign: 'center',
        height: 30,
        marginLeft: 20,
        letterSpacing: 0,
        paddingRight: 40,
    },
    topTextWrapper: {
        paddingVertical: 100,
        justifyContent: 'center',
        alignItems: 'center',
        width: width - 60,
    },
    bottomWrapper: {
        flexShrink: 1,
        width: width - 60,
        justifyContent:'center',
        alignItems: 'center',
    },
    topText: {
        fontSize: 16,
        fontFamily: 'robotoRegular',
        textAlign: 'center'
    },
    loginButtontext: {
        color: '#fff',
        fontSize: 20,
        fontFamily: 'robotoRegular',
    },
    loginButton: {
        paddingVertical: 15,
        width: width - 80,
        alignItems: 'center',
        backgroundColor: '#1c5a94',
        justifyContent: 'center',
        marginTop: 60,
        shadowOffset: {  
            width: 1,  
            height: 1,  
        }, 
        shadowColor: 'black',
        shadowOpacity: 0.3, 
        elevation: 2
    }
})

const mapStateToProps = (state) => ({
    otpStatus: state.login.otpStatus,
})

const mapDispatchToProps = (dispatch) => ({
    verifyOTP: (payload) => dispatch(verifyOTP(payload)),
})

export default connect(mapStateToProps, mapDispatchToProps)(EnterOTP);