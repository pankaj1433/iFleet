import FetchIntercept from '../utils/FetchIntercept'
import API from '../utils/ApiConstants';
import { WebBrowser} from 'expo';

import {getDocumentsList} from './documents.action';
import {showLoader, hideLoader} from './loader.action';

export const getDownloadToken = (documentID) => {
    return (dispatch) => {
        // dispatch(showLoader());
        FetchIntercept(`${API.REQUEST_DOWNLOAD_TOKEN}${documentID}`, { method: 'GET' })
        .then( res => {
            if(Object.keys(res).length > 0) {
                dispatch({type: 'ADD_DOWNLOAD_TOKEN', downloadToken: res.token});
                WebBrowser.openBrowserAsync(`http://52.221.210.155:8118${API.DOWNLOAD_FILE}${res.token}`)
                    .then((resp) => {
                        console.log("Finished", resp);
                        // dispatch(hideLoader());
                    })
                    .catch(error => {
                        alert.error(error);
                    });
            }
            else {
                console.log('File Not Found')
            }
        });
    };
};

export const deleteDocument = (documentID) => {
    return (dispatch) => {
        dispatch(showLoader());
        FetchIntercept(`${API.DELETE_DOCUMENT}${documentID}`, { method: 'DELETE' })
        .then( res => {
            if (Object.keys(res).length> 0 && Object.keys(res)[0] == "deleted") {
                dispatch(getDocumentsList());
                dispatch(hideLoader());
            }
        });
    };
}

export const uploadFile = (assetID,operationID,file) => {
    return (dispatch) => {
        dispatch(showLoader());
        FetchIntercept(`${API.UPLOAD_FILE}${assetID}/${operationID}`, { method: 'POST', headers: { 'content-type': 'multipart/form-data' },body:file })
        .then( res => {
                console.log('res----saveimg',res);
                dispatch({type: 'UPLOAD_FILE_SUCCESS', data: res});
                dispatch(hideLoader());
        });
    };
}