import FetchIntercept from '../utils/FetchIntercept'
import API from '../utils/ApiConstants';
import {showLoader, hideLoader} from './loader.action';

export const getAssetsList = (operationID) => {
    return (dispatch, getState) => {
        dispatch(showLoader());
        FetchIntercept(`${API.LIST_OPERATIONS}${operationID}`, { method: 'GET' })
        .then( res => {
            if (res.data && res.data.length > 0) {
                dispatch({ type: 'GET_ASSETS' , data : res.data});
            }
            dispatch(hideLoader());
        });
    };
};