import FetchIntercept from '../utils/FetchIntercept'
import API from '../utils/ApiConstants';
import {showLoader, hideLoader} from './loader.action';
import {logout} from './login.action';

export const getOperationList = () => {
    return (dispatch, getState) => {
        dispatch(showLoader());
        FetchIntercept(API.LIST_OPERATIONS, { method: 'GET' })
        .then( res => {
            if (res.data && res.data.length > 0) {
                dispatch({ type: 'GET_OPERATIONS' , data : res.data});
            }
            else {
                alert('some error occured');
                dispatch(logout());
            }
            dispatch(hideLoader());
        });
    };
};